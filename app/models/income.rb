class Income < ActiveRecord::Base
  belongs_to :category
  validates :category_id, presence: true
  validates :amount, presence: true, numericality: true
end
