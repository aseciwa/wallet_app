class Category < ActiveRecord::Base
  has_many :costs, dependent: :destroy
  has_many :incomes, dependent: :destroy
  validates :name, presence: true


  belongs_to :user

  # Get total income
  def total_incomes
    total = 0
    incomes.each |income|
        total += income.amount
    total
  end

  # Get cost total
  def total_costs
    total = 0
    costs.each do |cost|
      total += cost.amount
    end
    total
  end

end
