class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.integer :amount
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
